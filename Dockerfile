FROM registry.redhat.io/rhel8/nginx-116:1-95

ADD ./nginx.conf "${NGINX_CONF_PATH}"

COPY --chown=default:root ./build/ .

USER 0
RUN chown -R 1001:0 /opt/app-root/src

EXPOSE 8080

CMD nginx -g "daemon off;"
